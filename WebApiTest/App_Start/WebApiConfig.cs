﻿using System.IO;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApiTest
{
    public static class WebApiConfig
    {
        public static string Origins = null;
        public static void Register(HttpConfiguration config)
        {
            LoadWhiteList();

            var cors = new EnableCorsAttribute(Origins, "*", "*");
            config.EnableCors(cors);
           
            config.MapHttpAttributeRoutes();           
        }


        public static void LoadWhiteList()
        {
            if (Origins == null)
            {
                Origins = File.ReadAllText("C://Configurations/allow-origins.txt");
            }
        }
    }
}