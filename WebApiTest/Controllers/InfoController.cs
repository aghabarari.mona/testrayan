﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using WebApiTest.Models;

namespace WebApiTest
{
    public class InfoController : ApiController
    {
        private Dictionary<string, InformationModel> Info = new Dictionary<string, InformationModel>();
        
        [HttpPost]
        public string Add([FromBody] InformationModel model)
        {
            try
            {
                if (InformationModelIsValid(model))
                {
                    var info = new InformationModel();
                    info = model;
                    string id = Guid.NewGuid().ToString();
                    this.Info.Add(id, model);
                    return id;
                }
                return "اطلاعات وارد شده را بررسی و مجددا اقدام نمایید.";
            }
            catch (Exception e)
            {
                throw new Exception("خطا در ذخیره اطلاعات",e);
            }
        }


        [Route("{id}")]
        [HttpGet]
        public void GetInfo(string id)
        {

        }

        [Route("edit/{id}")]
        [HttpPost]
        public void Update(string id)
        {
            var item = this.Info[id];
            if (item != null)
            {
            }
        }
        
        private Boolean InformationModelIsValid(InformationModel model)
        {
            return true;
        }
    }
}