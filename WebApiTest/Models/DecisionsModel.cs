﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTest.Models
{
    public class DecisionsModel
    {
        public string PLPure { get; set; }
        public string PLBeginPeriod { get; set; }
        public string DisCount { get; set; }
        public string PLBeginPeriodDisCount { get; set; }
        public string SumLastYear { get; set; }
        public string ChangeCapital { get; set; }
        public string BeginPeriodProfit { get; set; }
        public string TransferFromOtherItem { get; set; }
        public string AllocateProfit { get; set; }
        public string TransferToLegalSave { get; set; }
        public string TransferToOtherSave { get; set; }
        public string PLEndPeriodDisCount { get; set; }
        public string SumCuttentYear { get; set; }
        public string PLEndPeriodDisCountWithDes { get; set; }
        public string PLPurePerShare { get; set; }
        public string ProfitPurePerShare { get; set; }
        public string Capital { get; set; }
    }
}