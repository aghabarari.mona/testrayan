﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTest.Models
{
    public class InformationModel
    {
        public string EndTime { get; set; }
        public string CompanyName { get; set; }
        public string Symbol { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string Day { get; set; }
        public string Date { get; set; }
        public string Address { get; set; }
        public string Order { get; set; }
        public List<AudienceModel> Audiences { get; set; }
        public List<MemberModel> Members { get; set; }
        public DecisionsModel Decisions { get; set; }
    }
}