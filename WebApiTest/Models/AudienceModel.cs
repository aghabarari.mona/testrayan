﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTest.Models
{
    public class AudienceModel
    {
        public string AudienceName { get; set; }
        public string AudienceCount { get; set; }
        public string AudiencePerCent { get; set; }
    }
}