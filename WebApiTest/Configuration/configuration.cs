﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApiTest.Configuration
{
    public class configuration
    {
        public static string Origins = null;
        public static void Register(HttpConfiguration config)
        {
            LoadWhiteList();

            var cors = new EnableCorsAttribute(Origins, "*", "*");
            config.EnableCors(cors);
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        public static void LoadWhiteList()
        {
            if (Origins == null)
            {
                Origins = File.ReadAllText("C://SdpConfigurations/crm-allow-origins.txt");
            }
        }
    }
}